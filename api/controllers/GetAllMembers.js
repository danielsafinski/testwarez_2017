import client from "../helpers/es"
import monitor from "../helpers/monitor";
import es from "./../../utils/es_index"


module.exports = {
  GetAllMembers : GetAllMembers
};

function GetAllMembers(req, res) {
  let start = monitor();
  client.search({
    index: es.es,
    type: es.es,
    q: '*',
    _sourceInclude: 'member_id, company, memberName, tags, datecreated, duedate, completed'
  }, (error, response) => {
    if(error){
      res.end(JSON.stringify(error));
    } else {
      let results = [];
      results = response.hits.hits.map((hit) => hit._source);
      res.header('Content-Type', 'application/json');
      res.end(JSON.stringify(results));
      monitor(start, 'GetAllMembers');
    }
  });
}
