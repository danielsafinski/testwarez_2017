import client from "../helpers/es"
import monitor from "../helpers/monitor";
import es from "./../../utils/es_index"

module.exports = {
    UpdateMemberById: UpdateMemberById
};

function UpdateMemberById(req, res) {
    let start = monitor();
    client.update({
        index: es.es,
        type: es.es,
        id: req.swagger.params.id.value,
        body: {
            doc: req.swagger.params.updated_member.value
        }
    }, (error, response) => {
        res.header('Content-Type', 'application/json');
        if (error) {
            res.statusCode = 400;
            res.end(JSON.stringify(error));
        } else {
            res.end(JSON.stringify(response));
            monitor(start, 'UpdateMemberById');
        }
    });
}
