import client from "../helpers/es"
import monitor from "../helpers/monitor";
import es from "./../../utils/es_index"

module.exports = {
    FindMemberById : FindMemberById
};

function FindMemberById(req, res) {
  let start = monitor();
  console.log(`Getting member with id ${req.swagger.params.id.value}`);
  client.get({
    index: es.es,
    type: es.es,
    id: req.swagger.params.id.value
  }, (error, response) => {
    res.header('Content-Type', 'application/json');
    if(error){
      res.end(JSON.stringify(error));
    } else {
      res.end(JSON.stringify(response._source));
      monitor(start, 'FindMemberById');
    }
  });
}
