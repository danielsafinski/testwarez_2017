import client from "../helpers/es"
import monitor from "../helpers/monitor";
import es from "./../../utils/es_index"

module.exports = {
  AddMember : AddMember
};

function AddMember (req, res) {
  let start = monitor();
  client.create({
    index: es.es,
    type: es.es,
    id: req.swagger.params.member.value.member_id,
    body: req.swagger.params.member.value
  }, (error,response) =>{
    res.header('Content-Type', 'application/json');
    if(error){
      console.log(error);
      res.statusCode = 409;
      res.end(JSON.stringify(error));
    } else {
      console.log(JSON.stringify({'id': req.swagger.params.member.value.member_id}));
      res.end(JSON.stringify({'body': req.swagger.params.member.value}));
      monitor(start, 'AddMember');
    }
  })
}
