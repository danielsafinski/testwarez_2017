import client from "../helpers/es"
import monitor from "../helpers/monitor";
import es from "./../../utils/es_index"

module.exports = {
  DeleteMemberById : DeleteMemberById
}

function DeleteMemberById(req, res) {
  let start = monitor();
  console.log(`Deleting member with id ${req.swagger.params.id.value}`);
  client.delete({
    index: es.es,
    type: es.es,
    id: req.swagger.params.id.value
  }, (error, response) =>{
    res.header('Content-Type', 'application/json');
    if(error){
      res.end(JSON.stringify(error));
    } else {
      res.end(JSON.stringify(response.result));
      monitor(start, 'DeleteMemberById');
    }
  });
}
