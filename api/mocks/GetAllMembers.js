'use strict';

module.exports = {
  GetAllMembers : GetAllMembers
}

function GetAllMembers(req, res, next) {
  res.json([
    {
      testwarez_id: 0,
      company: "Get some milk",
      member: "Jim",
      createddate: "2016-11-01T23:15:00.000Z",
      duedate: "2016-11-08T08:00:00.000Z",
      completed: false
    },
    {
      testwarez_id: 1,
      company: "Get some cereal",
      member: "Austin",
      createddate: "2016-11-01T23:15:00.000Z",
      duedate: "2016-11-08T08:00:00.000Z",
      completed: false
    }
  ])
}
