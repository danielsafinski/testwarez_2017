# Testwarez_2017 API Server built with [Swagger ](http://swagger.io/)

## Getting Started
- Clone the repo.
- Install dependencies with `npm install`
- Ensure you have swagger installed globally with `npm install swagger -g`
- Start the node.js application using Swagger with `swagger project start`
 - Ensure you have nodejs-dashboard installed with `npm install nodejs-dashboard -g`
- Launch the swagger editor to view/edit the API definition with `swagger project edit`

## Additional Info:
- Swagger NPM package can be found [here](https://www.npmjs.com/package/swagger)
- Elasticsearch NPM client used is [here](https://www.npmjs.com/package/elasticsearch)


## Running Elasticsearch and Sample Data:

Launch the elasticsearch instance:
`docker-compose up`

This will launch docker with an elasticsearch container and a kibana container (`http://localhost:5601`).

- Ensure you have nodejs-dashboard installed with `npm install babel-cli -g  `
Run the node command to import the sample data:
`babel-node utils/import.js`

You should receive success/confirmation messages for each item added to elasticsearch
Check to see that the `testwarez` index was created, it should be listed in the indices for the elasticsearch container:
`curl localhost:9200/_cat/indices?v`

If you want to see the data imported:
`curl localhost:9200/testwarez/_search&q=*`