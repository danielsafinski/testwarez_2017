require('babel-core/register');
import es from "./es_index"
const ELASTICSEARCH = require('elasticsearch');
const SAMPLES = require('./sample-testwarez.json');
const CLUSTER = 'http://localhost:9200';
const INDEX = es.es;
const TYPE = es.es;
const CLIENT = new ELASTICSEARCH.Client({
    host: CLUSTER,
    apiVersion: '5.0'
});

SAMPLES.forEach((sample) => {
    console.log(`Saving member ${sample.memberName} to Elasticsearch`);
    CLIENT.index({
        index: INDEX,
        type: TYPE,
        id: sample.member_id,
        body: sample
    }, (error, response) => {
        if(error) {
            console.log('DOH! Something went wrong!');
        } else {
            console.log(response.result);
        }
    });
});