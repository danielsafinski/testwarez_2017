import chakram from "chakram";
import api from "../helpers/api";
import headers from "../helpers/headers";
import {addMemberPostdata} from "../helpers/postdata";

export const addMember = (member_id, company, memberName, datecreated, duedate, completed) =>
    new Promise((resolve, reject) =>
        chakram.post(api.testwarez, addMemberPostdata({
            member_id,
            company,
            memberName,
            datecreated,
            duedate,
            completed
        }), headers)
            .then((response) => {
                if (response.response.statusCode === 200) {
                    const member_id = response.body.body.member_id;
                    console.info("MEMBER_ID:", response.body.body.member_id);
                    resolve({member_id});
                } else {
                    console.error(response.body);
                    reject("Please debug.");
                }
                return chakram.wait();
            }));