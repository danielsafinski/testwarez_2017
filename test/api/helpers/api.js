const endpoints = {
    test: {
        testwarez: "http://localhost:10010/",
    },
    dev: {
        testwarez: "http://localhost:10010/",
    },
    localhost: {
        testwarez: "http://localhost:10010/"
    },
};

export default endpoints[process.env.NODE_ENV];
