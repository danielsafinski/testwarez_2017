import chakram from "chakram";

const expect = chakram.expect;

export const expectSuccess = (response) => {
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", /json/);
};

export const expectSuccessWithSchema = (response, schema) => {
    expectSuccess(response);
    expect(response).to.have.schema(schema);
};
