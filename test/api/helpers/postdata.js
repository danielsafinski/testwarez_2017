import moment from "moment"
import randomInt from "random-int";
import randomWords from "random-words";

const ID = randomInt(0, 3000),
    STRING = randomWords(2);

export const addMemberPostdata = ({member_id, company, memberName, datecreated, duedate, completed}) => ({
        member_id: typeof member_id === "undefined" ? ID : member_id,
        company: typeof company === "undefined" ? STRING[0] : company,
        memberName: typeof memberName === "undefined" ? STRING[1] : memberName,
        datecreated: typeof datecreated === "undefined" ? moment().format() : datecreated,
        duedate: typeof duedate === "undefined" ? moment().format() : duedate,
        completed: typeof completed === "undefined" ? false : completed,
});