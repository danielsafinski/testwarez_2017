/** ćwiczenie: stwórz własnego użytkownika i wykonsoluj swoje dane.**/

import chakram from "chakram";
import api from "../helpers/api";
import headers from "../helpers/headers";
import {addMemberPostdata} from "../helpers/postdata";
import { expectSuccess } from "../helpers/assertions";
import randomInt from "random-int";


const ID = randomInt(0, 3000);

describe('Add Testwarez_2017 member. ', function () {

    it('Should add member', () => {
        return chakram.post(api.testwarez, addMemberPostdata({member_id: ID}), headers)
            .then((response) => {
                console.info(`Id from new member: ${response.body.body.member_id}`);
                expectSuccess(response);
                return chakram.wait();
            });
    });
});
