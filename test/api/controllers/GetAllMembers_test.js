import chakram from "chakram";
import api from "../helpers/api";

const expect = chakram.expect;

describe('Get all members.', function () {
    it('should get all members', () => {
        return chakram.get(api.testwarez)
            .then((response) => {
                expect(response).to.have.status(200);
                expect(response).to.have.header("content-type", /json/);
                return chakram.wait();
            });
    });
});
