import chakram from "chakram";
import api from "../helpers/api";
import { addMember } from "../helpers/steps";
import schema from "../schema/TestWarez-Schema.json"
import { expectSuccessWithSchema } from "../helpers/assertions";

const expect = chakram.expect;

describe('Find TestWarez_2017 member.', function () {

    let memberId;

    before("Add member.",function (){
        return addMember().then((res)=>{
            memberId = res.member_id;
        })
    })


    it('should get member by Id.', () => {
        return chakram.get(api.testwarez + `testwarez/${memberId}`)
            .then((response) => {
                // console.log(`Member object: ${JSON.stringify(response.body)}`)
                console.log("Member object:", response.body);
                const memberIdResponse = response.body.member_id;

                expectSuccessWithSchema(response, schema)
                // expect(response).to.have.status(200);
                // expect(response).to.have.header("content-type", /json/);
                // expect(response).to.have.schema(schema);
                expect(memberIdResponse).to.be.equal(memberId, "Your member id is wrong.");

                return chakram.wait();
            });
     });
});
