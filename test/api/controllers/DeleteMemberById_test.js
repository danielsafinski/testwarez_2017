/** ćwiczenie: Zrób expecta na response.body **/

import chakram from "chakram";
import api from "../helpers/api";
import { addMember } from "../helpers/steps";

const expect = chakram.expect;

describe('Delete TestWarez_2017 member.', function () {

    let memberId;

    before("Add member.",function (){
        return addMember().then((res)=>{
            memberId = res.member_id;
        })
    });


    it('should delete member by Id.', () => {
        return chakram.delete(api.testwarez + `testwarez/${memberId}`)
            .then((response) => {
                console.log("Deleted Message: ", response.body);
                expect(response).to.have.status(200);
                return chakram.wait();
            });
    });
});
